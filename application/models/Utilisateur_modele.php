<?php

/*
 * Classe utilisateur_modele qui hérite de CI_Model
 * @author: Tounsi / Riotor
 * @version : 1.0.1
 */

class utilisateur_modele extends CI_Model {
    /*
     * Fonction constructeur
     */

    public function __construct() {
        $this->load->database();
    }

    /*
     * Fonction get_utilisateur qui permet de sélectionner l'identifiant de l'adherent
     * et le retourne 
     * 
     */

    public function get_utilisateur($idadh) {
        $query = $this->db->get_where('adherent', array('idadh' => $idadh));
        return $query->result_array();
    }

    /*
     * fonction set_utilisateur qui permet d'insérer les donnes de $data qui viennent du formulaire
     * dans la base de données adherent.
     */

    public function set_utilisateur() {
        $data = array(
            'nomadh' => $this->input->post('nomadh'),
            'prenomadh' => $this->input->post('prenomadh'),
            'login' => $this->input->post('login'),
            'mdp' => $this->input->post('mdp'),
            'ville' => $this->input->post('ville'),
            'codepostal' => $this->input->post('codepostal'),
            'adresse' => $this->input->post('adresse')
            //'idadh' => 'default'
        );
        return $this->db->insert('adherent', $data);
    }

    public function login($login, $mdp) {
        $this->db->select('idadh, login, mdp');
        $this->db->from('adherent');
        $this->db->where('login', $login);
        $this->db->where('mdp', $mdp);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    

        public function get_login($login) {
        $query = $this->db->query("SELECT * FROM adherent WHERE login = '$login' LIMIT 1");
        return $query->row_array();
    }
        public function changer_mdp($login, $mdp1) {
        
        $query = $this->db->query("UPDATE adherent SET mdp= '$mdp1' WHERE login = '$login'");
    }
    
    
    
    /*
		Fonction getReservationEnCours permettant de retourner les réservations qui ne sont pas encore accepté par l'administrateur en fonction d'un client dont le numéro est passé en paramètre
    */
    public function getReservationEnCours($idClient)
		{
			$this->db->select('*');
			$this->db->from('reservation');
                        $this->db->join('logement', 'reservation.fk_idlogement = logement.idlogement');
			$this->db->where(array(
                            'etatreservation' => "En cours ...",
                            'fk_idadh' => $idClient
                            ));
			$query = $this->db->get();
			return $query->result_array();
		}

		/*
			Fonction getReservationValide permettant de retourner les réservations acceptées en fonction d'un client dont le numéro est passé en paramètre
		*/
		public function getReservationValide($idClient)
		{
			$this->db->select('*');
			$this->db->from('reservation');
			$this->db->where(array(
                            'etatreservation' => "Valide",
                            'fk_idadh' => $idClient
                                ));
			$query = $this->db->get();
			return $query->result_array();
		}

		/*
			Fonction deleteReservation permettant de supprimer une réservation en fonction d'un numéro
		*/
		public function deleteReservation($num)
		{
			$this->db->delete('reservation', array('numreservation' => $num));
		}

		/*
			Fonction setReservation permettant l'insertion des demandes de réservations dans la base de données avec gestion des différentes erreurs en prenant en paramètre l'identifiant de l'adhérent connecté.
		*/
		public function setReservation($num){

			$datedebut = $this->input->post('datedebut');



			$dateformat = 'Y-m-d';
			$arraydate = date_parse_from_format($dateformat, $datedebut);
			$moredays = 7;
			$datefin = date(
				$dateformat,
				mktime(0,0,0, $arraydate['month'], $arraydate['day'] + $moredays, $arraydate['year']));



			$datereserv = date('Y-m-d');
			$mob = $this->input->post('mobilite');
			$menage = $this->input->post('menage');
			$nbpers = $this->input->post('nbpers');
			$restauration = $this->input->post('restauration');
                        $fk_idlogement = $this->input->post('fk_categ');
			//$fk_categ = $this->input->post('fk_categ');
			$fk_idadh = $num;

			if(is_null($mob)){
				$mob = "off";
			}

			if(is_null($menage)){
				$menage = "off";
			}


			$tabDate = explode('-', $datedebut);
			$timestamp = mktime(0, 0, 0, $tabDate[1], $tabDate[2], $tabDate[0]);
			$jour = date('w', $timestamp);
			if($jour != 6)
			{
				return "Les réservations ne se font que du Samedi au Samedi.";
			}


			$this->db->select('datedebut, fk_idlogement');
			$this->db->from('reservation');
			$this->db->where(array('datedebut' => $datedebut));
			$query = $this->db->get();
			foreach ($quest = $query->result_array() as $row) {
				if($row['datedebut'] == $datedebut && $row['fk_idlogement'] == $fk_idlogement){
					return "Il y a déjà une réservation à la date du ".$datedebut." pour ce logement. </br>Votre demande n'a pas été prise en compte.";
				}
			}

				$data = array(
	                'datedebut' => $datedebut,
	                'datefin' => $datefin,
	                'menage' => $menage,
	                'etatreservation' => "En cours ...",
	                'typereservation' => "Formulaire internet",
	                'mobilitereduite' => $mob,
	                'fk_idlogement' => $fk_idlogement,
	                'fk_idadh' => $fk_idadh,
					'datereservation' => $datereserv,
					'nbpersonne' => $nbpers,
					'restauration' => $restauration       
				);

				return $this->db->insert('reservation', $data);
}
}
?>